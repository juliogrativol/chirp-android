package com.njinformatica.chirp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunctionException;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.amazonaws.regions.Regions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.njinformatica.chirp.lambda.BuscarSalaRequest;
import com.njinformatica.chirp.lambda.BuscarSalaResponse;
import com.njinformatica.chirp.lambda.ILambda;

public class MainActivity extends AppCompatActivity {

    ProgressDialog dialog = null;
    EditText textView = null;
    ApplicationSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        settings = ((ApplicationSettings) getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        settings.clean();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d("Executed app", "Saiu da aplicação main" );
        //encerrarSala();
        Log.d("Executed app", "Encerrou a sala" );
    }

    @Override
    public void onDetachedFromWindow() {
        Log.d("Executed app", "onDetachedFromWindow" );
    }

    public void onButtonClickEntrar(View view){

        EditText textView = (EditText)findViewById(R.id.editText);

        if (textView.getText().toString() == null || textView.getText().toString().equals("")){
            AlertHelper.criaSimpleAlert(MainActivity.this,"Atenção" ,"Preencha a identificação da sala!");
        }else{
            buscaSala();
        }
    }

    private void buscaSala(){

        if (checkInternetConnection()) {

            CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                    this.getApplicationContext(), "us-east-1:f312a220-7796-462e-9513-2a9ced3b83f4", Regions.US_EAST_1);

            LambdaInvokerFactory factory = new LambdaInvokerFactory(this.getApplicationContext(),
                    Regions.US_EAST_1, cognitoProvider);

            final ILambda myInterface = factory.build(ILambda.class);

            textView = (EditText) findViewById(R.id.editText);

            BuscarSalaRequest request = new BuscarSalaRequest(textView.getText().toString());

            dialog = ProgressDialog.show(MainActivity.this, "",
                    "Carregando...", true);

            try {

                new AsyncTask<BuscarSalaRequest, Void, BuscarSalaResponse>() {
                    @Override
                    protected BuscarSalaResponse doInBackground(BuscarSalaRequest... params) {
                        // invoke "echo" method. In case it fails, it will throw a
                        // LambdaFunctionException.
                        try {
                            return myInterface.buscarSala(params[0]);
                        } catch (LambdaFunctionException lfe) {
                            Log.e("Tag", "Failed to invoke echo", lfe);
                            AlertHelper.criaSimpleAlert(MainActivity.this, "Falha", "Falha ao buscar a sala!");
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(BuscarSalaResponse result) {
                        if (result == null) {
                            AlertHelper.criaSimpleAlert(MainActivity.this, "Falha", "Falha ao buscar a sala!");
                            return;
                        }

                        dialog.dismiss();

                        if (result.getCodigo().equalsIgnoreCase("0")) {
                            settings.setSalaAtiva(true);
                            settings.setSala(result.getId());
                            settings.setOwner(false);
                            settings.setTokenFMC(FirebaseInstanceId.getInstance().getToken());
                            settings.setSalaOwnerTokenFMC(result.getResponsavel());
                            chameSalaActivity(result.getTempoRestante(), textView.getText().toString());
                        } else {
                            AlertHelper.criaSimpleAlert(MainActivity.this, "Falha",result.getMensagem());
                        }
                    }
                }.execute(request);
            }catch (Exception e){
                AlertHelper.criaSimpleAlert(MainActivity.this, "Falha", "Falha de conexão com a internet!");
            }
        }else{
            AlertHelper.criaSimpleAlert(MainActivity.this, "Falha", "Falha de conexão com a internet!");
        }
    }

    public void onButtonClickCriar(View view){
        Intent i = new Intent(MainActivity.this, CriarActivity.class);
        startActivity(i);
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Log.v("TAG", "Internet Connection Not Present");
            return false;
        }
    }

    public void chameSalaActivity(String tempoRestante, String sala) {
        Intent i = new Intent(MainActivity.this, SalaActivity.class);
        i.putExtra("tempoRestante", tempoRestante);

        settings.setSala(sala);
        settings.setOwner(false);

        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        Log.d("Executed app", "Botão voltar desabilitado!" );
    }

}
