package com.njinformatica.chirp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunctionException;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.amazonaws.regions.Regions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.njinformatica.chirp.lambda.CriarSalaRequest;
import com.njinformatica.chirp.lambda.CriarSalaResponse;
import com.njinformatica.chirp.lambda.ILambda;

public class CriarActivity extends AppCompatActivity {

    ProgressDialog dialog = null;
    int tempoEscolhido = 5;
    ApplicationSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        settings = ((ApplicationSettings)getApplicationContext());

        NumberPicker np = (NumberPicker) findViewById(R.id.numberPicker);

        final String[] values= {"05","10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "65", "70", "75", "80", "85", "90", "95", "100", "105", "110", "115", "120"};

        np.setMinValue(0);
        np.setMaxValue(values.length-1);
        np.setDisplayedValues(values);
        np.setWrapSelectorWheel(true);

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                tempoEscolhido = Integer.valueOf(values[newVal]);
            }
        });
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Log.v("TAG", "Internet Connection Not Present");
            return false;
        }
    }

    private void criarSala(){

        if (checkInternetConnection()) {

            try {

                CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                        this.getApplicationContext(), "us-east-1:f312a220-7796-462e-9513-2a9ced3b83f4", Regions.US_EAST_1);

                LambdaInvokerFactory factory = new LambdaInvokerFactory(this.getApplicationContext(),
                        Regions.US_EAST_1, cognitoProvider);

                final ILambda myInterface = factory.build(ILambda.class);

                CriarSalaRequest request = new CriarSalaRequest(String.valueOf(tempoEscolhido), FirebaseInstanceId.getInstance().getToken());

                dialog = ProgressDialog.show(CriarActivity.this, "",
                        "Carregando...", true);



                new AsyncTask<CriarSalaRequest, Void, CriarSalaResponse>() {
                    @Override
                    protected CriarSalaResponse doInBackground(CriarSalaRequest... params) {
                        // invoke "echo" method. In case it fails, it will throw a
                        // LambdaFunctionException.
                        try {
                            return myInterface.criarSala(params[0]);
                        } catch (LambdaFunctionException lfe) {
                            Log.e("Tag", "Failed to invoke echo", lfe);
                            AlertHelper.criaSimpleAlert(CriarActivity.this, "Falha", "Falha ao criar a sala!");
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(CriarSalaResponse result) {
                        if (result == null) {
                            AlertHelper.criaSimpleAlert(CriarActivity.this, "Falha", "Falha ao criar a sala!");
                            return;
                        }

                        dialog.dismiss();

                        if (result.getCodigo().equalsIgnoreCase("0")) {
                            settings.setSalaAtiva(true);
                            settings.setSala(result.getId());
                            settings.setOwner(false);
                            settings.setTokenFMC(FirebaseInstanceId.getInstance().getToken());
                            settings.setSalaOwnerTokenFMC(result.getResponsavel());
                            chameSalaActivity(String.valueOf(tempoEscolhido * 60), result.getId());
                        } else {
                            AlertHelper.criaSimpleAlert(CriarActivity.this, "Falha", "Falha ao criar a sala!");
                        }
                    }
                }.execute(request);
            } catch (Exception e) {
                dialog.cancel();
                AlertHelper.criaSimpleAlert(CriarActivity.this, "Falha", "Falha de conexão com a internet!");
            }
        }else{
            AlertHelper.criaSimpleAlert(CriarActivity.this, "Falha", "Falha de conexão com a internet!");
        }
    }

    public void onButtonClickCriar(View view){
        criarSala();
    }

    public void chameSalaActivity(String tempoRestante, String sala){
        Intent i = new Intent(CriarActivity.this, SalaActivity.class);
        i.putExtra("tempoRestante", tempoRestante);
        startActivity(i);

        settings.setSala(sala);
        settings.setOwner(true);
    }
}
