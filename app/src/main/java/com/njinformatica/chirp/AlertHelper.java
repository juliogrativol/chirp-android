package com.njinformatica.chirp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by jthomaz on 31/01/2018.
 */

public class AlertHelper {
    public static void criaSimpleAlert(Activity activity, String title, String message){
        AlertDialog alertDialog = new AlertDialog.Builder(
                activity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }
}
