package com.njinformatica.chirp;

import android.app.Application;

/**
 * Created by jthomaz on 02/02/2018.
 */

public class ApplicationSettings extends Application {
    private boolean isOwner = false;
    private String sala;
    private String tokenFMC;
    private String salaOwnerTokenFMC;
    private boolean isSalaAtiva = false;

    public boolean isSalaAtiva() {
        return isSalaAtiva;
    }

    public void setSalaAtiva(boolean salaAtiva) {
        isSalaAtiva = salaAtiva;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getTokenFMC() {
        return tokenFMC;
    }

    public void setTokenFMC(String tokenFMC) {
        this.tokenFMC = tokenFMC;
    }

    public String getSalaOwnerTokenFMC() {
        return salaOwnerTokenFMC;
    }

    public void setSalaOwnerTokenFMC(String salaOwnerTokenFMC) {
        this.salaOwnerTokenFMC = salaOwnerTokenFMC;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }

    public void clean(){
        setSala(null);
        setOwner(false);
        setSalaOwnerTokenFMC(null);
        setTokenFMC(null);
        setSalaAtiva(false);
    }
}
