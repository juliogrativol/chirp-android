package com.njinformatica.chirp;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunctionException;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.amazonaws.regions.Regions;
import com.njinformatica.chirp.lambda.EncerrarSalaRequest;
import com.njinformatica.chirp.lambda.EncerrarSalaResponse;
import com.njinformatica.chirp.lambda.ILambda;
import com.njinformatica.chirp.lambda.NotificarResponsavelRequest;
import com.njinformatica.chirp.lambda.NotificarResponsavelResponse;


import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SalaActivity extends AppCompatActivity {

    MediaPlayer mp = null;
    AudioManager audioManager = null;
    MediaPlayer mpNotification = null;
    AudioManager audioManagerNotification = null;
    boolean acabou = false;
    Long tempoRestante = null;
    ProgressDialog dialog = null;
    ApplicationSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sala);

        settings = ((ApplicationSettings)getApplicationContext());

        tempoRestante = new Long(this.getIntent().getExtras().getString("tempoRestante"));

        TextView np = (TextView) findViewById(R.id.textView5);
        np.setText(settings.getSala());

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        settings.setSalaAtiva(true);

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getRunningApps();
            }
        }, 0, 300);

        startTime();
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Log.v("TAG", "Internet Connection Not Present");
            return false;
        }
    }

    protected void startTime(){
        CountDownTimer cT =  new CountDownTimer(tempoRestante*1000, 1000) {

            TextView textView = (TextView)findViewById(R.id.textView2);

            public void onTick(long millisUntilFinished) {

                String minutos = String.format("%02d", millisUntilFinished/60000);
                int segundos = (int)( (millisUntilFinished%60000)/1000);
                textView.setText(minutos+":"+String.format("%02d",segundos));
            }

            public void onFinish() {
                textView.setText("Acabou!");
                acabou = true;
                settings.setSalaAtiva(false);
            }
        };
        cT.start();
    }

    public void getRunningApps() {
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> recentTasks = activityManager.getRunningAppProcesses();

        for (int i = 0; i < recentTasks.size(); i++)
        {
            if (recentTasks.get(i).pkgList[0].equalsIgnoreCase("com.njinformatica.chirp")) {
                if (recentTasks.get(i).importance == 400) {
                    Log.d("Executed app", "Application executed : " + recentTasks.get(i).pkgList[0] + "\t\t ID: " + recentTasks.get(i).pid + "   -------   importance: " + recentTasks.get(i).importance + "");

                    if (!acabou && settings.isSalaAtiva()) {
                        playSound();
                    }
                } else {
                    stopSound();
                }
            }
        }
    }

    @Override
    public void onResume(){
        stopSound();
        super.onResume();
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("myFunction"));
    }

    public void stopSound() {
        if (mp != null) {
            mp.stop();
            mp= null;
        }
    }

    public void stopSoundNotification() {
        if (mpNotification != null) {
            mpNotification.stop();
            mpNotification= null;
        }
    }

    protected void playSound() {
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

        if (mp == null) {
            mp = MediaPlayer.create(this, R.raw.bird);
            mp.start();

            notificar();
        }
    }

    protected void playSoundNotification() {
        audioManagerNotification = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManagerNotification.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

        if (mpNotification == null) {
            mpNotification = MediaPlayer.create(this, R.raw.bird);
            mpNotification.start();
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("Executed app", "Botão voltar desabilitado!" );
    }

    public void onButtonClickSair(View view){

        if (settings.isOwner()){
            if (settings.isSalaAtiva()) {
                new AlertDialog.Builder(this)
                        .setMessage("A reunião ainda não acabou. Tem certeza que deseja encerrar?")
                        .setCancelable(false)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                encerrarSala();

                                Intent i = new Intent(SalaActivity.this, MainActivity.class);
                                startActivity(i);

                                settings.clean();
                            }
                        })
                        .setNegativeButton("Não", null)
                        .show();
            }else{
                Intent i = new Intent(SalaActivity.this, MainActivity.class);
                startActivity(i);
                settings.clean();
            }

        }else {
            if (!acabou) {
                new AlertDialog.Builder(this)
                        .setMessage("A reunião ainda não acabou. Tem certeza que deseja sair?")
                        .setCancelable(false)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                notificar();

                                Intent i = new Intent(SalaActivity.this, MainActivity.class);
                                startActivity(i);
                                settings.clean();
                            }
                        })
                        .setNegativeButton("Não", null)
                        .show();
            } else {
                Intent i = new Intent(SalaActivity.this, MainActivity.class);
                startActivity(i);
                settings.clean();
            }
        }
    }

    public void encerrarSala(){

        dialog = ProgressDialog.show(SalaActivity.this, "",
                "Encerrando...", true);

        CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                this.getApplicationContext(), "us-east-1:f312a220-7796-462e-9513-2a9ced3b83f4", Regions.US_EAST_1);

        LambdaInvokerFactory factory = new LambdaInvokerFactory(this.getApplicationContext(),
                Regions.US_EAST_1, cognitoProvider);

        final ILambda myInterface = factory.build(ILambda.class);

        EncerrarSalaRequest request = new EncerrarSalaRequest(settings.getSala());

        dialog = ProgressDialog.show(SalaActivity.this, "",
                "Carregando...", true);

        if (checkInternetConnection()) {
            try {

                new AsyncTask<EncerrarSalaRequest, Void, EncerrarSalaResponse>() {
                    @Override
                    protected EncerrarSalaResponse doInBackground(EncerrarSalaRequest... params) {
                        // invoke "echo" method. In case it fails, it will throw a
                        // LambdaFunctionException.
                        try {
                            return myInterface.encerrar(params[0]);
                        } catch (LambdaFunctionException lfe) {
                            Log.e("Tag", "Failed to invoke echo", lfe);
                            AlertHelper.criaSimpleAlert(SalaActivity.this, "Falha", "Falha ao encerrar a sala!");
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(EncerrarSalaResponse result) {
                        if (result == null) {
                            AlertHelper.criaSimpleAlert(SalaActivity.this, "Falha", "Falha ao encerrar a sala!");
                            return;
                        }

                        dialog.dismiss();

                        if (result.getCodigo().equalsIgnoreCase("0")) {
                            Intent i = new Intent(SalaActivity.this, MainActivity.class);
                            startActivity(i);
                        } else {
                            AlertHelper.criaSimpleAlert(SalaActivity.this, "Falha", "Falha ao encerrar a sala!");
                        }

                    }
                }.execute(request);
            }catch (Exception e){
                AlertHelper.criaSimpleAlert(SalaActivity.this, "Falha", "Falha de conexão com a internet!");
            }
        }else{
            Log.e("Falha", "Falha ao notificar encerrar sala por causa de falta de conexão com a internet.");
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            playSoundNotification();
            String t = intent.getStringExtra("mensagem");

            new AlertDialog.Builder(SalaActivity.this)
                    .setMessage("Alguém perdeu o foco!")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            stopSoundNotification();
                        }
                    })
                    .show();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this.getApplicationContext()).unregisterReceiver(mMessageReceiver);
    }

    private void notificar(){
        CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                this.getApplicationContext(), "us-east-1:f312a220-7796-462e-9513-2a9ced3b83f4", Regions.US_EAST_1);

        LambdaInvokerFactory factory = new LambdaInvokerFactory(this.getApplicationContext(),
                Regions.US_EAST_1, cognitoProvider);

        final ILambda myInterface = factory.build(ILambda.class);

        NotificarResponsavelRequest request = new NotificarResponsavelRequest(settings.getSalaOwnerTokenFMC());

        if (checkInternetConnection()) {

            try {

                new AsyncTask<NotificarResponsavelRequest, Void, NotificarResponsavelResponse>() {
                    @Override
                    protected NotificarResponsavelResponse doInBackground(NotificarResponsavelRequest... params) {
                        // invoke "echo" method. In case it fails, it will throw a
                        // LambdaFunctionException.
                        try {
                            return myInterface.notificar(params[0]);
                        } catch (LambdaFunctionException lfe) {
                            Log.e("Tag", "Failed to invoke echo", lfe);
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(NotificarResponsavelResponse result) {
                        if (result == null) {
                            return;
                        }

                        if (result.getCodigo().equalsIgnoreCase("0")) {
                            Log.d("Notificando", "Notificando dono da sala");
                        } else {
                            Log.d("Notificando", "Falha ao notificar dono da sala");
                        }
                    }
                }.execute(request);
            } catch (Exception e) {
                Log.d("Notificando", "Falha ao notificar dono da sala por causa de falta de conexão com a internet.");
            }
        }else{
            Log.e("Falha", "Falha ao notificar dono da sala por causa de falta de conexão com a internet.");
        }
    }
}