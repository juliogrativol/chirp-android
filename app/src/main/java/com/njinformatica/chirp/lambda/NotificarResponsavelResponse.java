package com.njinformatica.chirp.lambda;

/**
 * Created by jthomaz on 15/03/2018.
 */

public class NotificarResponsavelResponse {

    private String codigo;
    private String mensagem;

    public NotificarResponsavelResponse() {

    }

    public NotificarResponsavelResponse(String codigo, String mensagem) {
        this.codigo = codigo;
        this.mensagem = mensagem;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}


