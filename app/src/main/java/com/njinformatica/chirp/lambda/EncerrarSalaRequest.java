package com.njinformatica.chirp.lambda;

/**
 * Created by jthomaz on 15/03/2018.
 */

public class EncerrarSalaRequest {

    private String id;

    public EncerrarSalaRequest(String id) {
        this.id = id;
    }

    public EncerrarSalaRequest() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
