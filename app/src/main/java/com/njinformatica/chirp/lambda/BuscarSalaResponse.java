package com.njinformatica.chirp.lambda;

/**
 * Created by jthomaz on 15/03/2018.
 */

public class BuscarSalaResponse {

    private String id;
    private String inicio;
    private String tempo;
    private String responsavel;
    private String ativa;
    private String tempoRestante;
    private String codigo;
    private String mensagem;

    public BuscarSalaResponse(String id, String inicio, String tempo, String responsavel, String ativa, String tempoRestante, String codigo, String mensagem) {
        this.id = id;
        this.inicio = inicio;
        this.tempo = tempo;
        this.responsavel = responsavel;
        this.ativa = ativa;
        this.tempoRestante = tempoRestante;
        this.codigo = codigo;
        this.mensagem = mensagem;
    }

    public BuscarSalaResponse() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getAtiva() {
        return ativa;
    }

    public void setAtiva(String ativa) {
        this.ativa = ativa;
    }

    public String getTempoRestante() {
        return tempoRestante;
    }

    public void setTempoRestante(String tempoRestante) {
        this.tempoRestante = tempoRestante;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
