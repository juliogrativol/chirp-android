package com.njinformatica.chirp.lambda;

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction;

public interface ILambda {

    /**
     * Invoke the Lambda function "AndroidBackendLambdaFunction".
     * The function name is the method name.
     */
    @LambdaFunction
    CriarSalaResponse criarSala(CriarSalaRequest request);

    @LambdaFunction
    BuscarSalaResponse buscarSala(BuscarSalaRequest request);

    @LambdaFunction
    NotificarResponsavelResponse notificar(NotificarResponsavelRequest request);

    @LambdaFunction
    EncerrarSalaResponse encerrar(EncerrarSalaRequest request);

}