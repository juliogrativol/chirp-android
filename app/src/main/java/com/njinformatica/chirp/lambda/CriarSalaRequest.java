package com.njinformatica.chirp.lambda;

/**
 * Created by jthomaz on 14/03/2018.
 */

public class CriarSalaRequest {

    private String tempo;
    private String responsavel;

    public CriarSalaRequest() {

    }

    public CriarSalaRequest(String tempo, String responsavel) {
        this.tempo = tempo;
        this.responsavel = responsavel;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
}