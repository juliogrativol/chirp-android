package com.njinformatica.chirp.lambda;

/**
 * Created by jthomaz on 15/03/2018.
 */

public class NotificarResponsavelRequest {

    private String responsavel;

    public NotificarResponsavelRequest() {

    }

    public NotificarResponsavelRequest(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
}
