package com.njinformatica.chirp.lambda;

/**
 * Created by jthomaz on 15/03/2018.
 */

public class BuscarSalaRequest {

    private String id;

    public BuscarSalaRequest(String id) {
        this.id = id;
    }

    public BuscarSalaRequest() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
