package com.njinformatica.chirp;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import java.util.Map;

/**
 * Created by jthomaz on 15/03/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override public void onMessageReceived(RemoteMessage remoteMessage)
    {
        String mensagem = "";

        Log.i("PVL", "Mensagem Recebida");
        if (remoteMessage.getData() != null) {
            Log.i("PVL", "Mensagem recebida:");
            mensagem = remoteMessage.getData().get("detail");
        } else {
            Log.i("PVL", "Mensagem recebida: " + remoteMessage.getNotification().getBody());
        }

        Intent intent = new Intent("myFunction");
        intent.putExtra("mensagem", mensagem);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}

